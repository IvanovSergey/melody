#include "gamewindow.h"
#include "ui_gamewindow.h"
#include <QDebug>
#include <QApplication>
#include <QDebug>
#include <QString>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QMediaPlayer>
#include <QDir>
#include <QResource>
#include <QSound>
#include <QSerialPort>




GameWindow::GameWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GameWindow)
{
    ui->setupUi(this);
    ui->lTime->setVisible(false);
    ui->lcdTime->setVisible(false);
}

void GameWindow::supergame(int t)
{
    ui->lTime->setVisible(true);
    ui->lcdTime->setVisible(true);
    ui->lcdTime->display(t);
}


GameWindow::~GameWindow()
{
    delete ui;
}

void GameWindow::addpoint(int a, int b)
{
    switch(a)
    {
        case 1:
            ui->lcdGamer1->display(ui->lcdGamer1->intValue()+b);
            break;
        case 2:
            ui->lcdGamer2->display(ui->lcdGamer2->intValue()+b);
            break;
        case 3:
            ui->lcdGamer3->display(ui->lcdGamer3->intValue()+b);
            break;
        default :
            qDebug("OLOLO!");
    }
}


void GameWindow::select(int a)
{
    switch(a)
    {
        case 1:
            ui->lGamer1->setStyleSheet("color: red;");
            break;
        case 2:
            ui->lGamer2->setStyleSheet("color: red;");
            break;
        case 3:
            ui->lGamer3->setStyleSheet("color: red;");
            break;
        default :
            ui->lGamer1->setStyleSheet("");
            ui->lGamer2->setStyleSheet("");
            ui->lGamer3->setStyleSheet("");
    }
}

void GameWindow::pressitemslot(int i, int j)
{
    ui->box->itemAtPosition(i,j)->widget()->setDisabled(true);
}
void GameWindow::myslot(QString s)
{
    qDebug() << "Сигнал открытия директории"<< s;
    if(!(ui->box->isEmpty()))
    {
        QLayoutItem *child;
        while ((child = ui->box->takeAt(0)) != 0)
            delete child->widget();

    }
    QDir dir,tmpdir;
    QString tmpstr;
    QLabel* label_melody;
    QLabel *label;
    QLabel *lnumber;
    dir.setPath(s);
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    for(unsigned int i = 0 ; i < dir.count(); i++)
    {
        label = new QLabel(dir[i]);

        label->setStyleSheet("color: DARKBLUE; font-size: 18pt;");

        ui->box->addWidget(label, i, 0);

        tmpstr = dir[i];
        tmpdir.setPath(s);
        tmpdir.cd(tmpstr);
        tmpdir.setFilter(QDir::Files | QDir::NoDotAndDotDot);
        for(unsigned int j = 0; j < tmpdir.count(); j++)
        {
            QString tmpname = "Мелодия ";
            tmpname += QString::number(j+1);
            label_melody = new QLabel(tmpname);
            label_melody->setPixmap(QPixmap(":img/melody.png"));
            ui->box->addWidget(label_melody, i, j + 1);
            lnumber = new QLabel(QString::number(j+1));
            lnumber->setStyleSheet("color: YELLOW; font-size: 26pt;");
            ui->box->addWidget(lnumber, i, j + 1);
        }
    }
    ui->box->update();
    dir = 0;
}



