#include "mainwindow.h"
#include "gamewindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
#include <QMediaPlayer>
#include "savethread.h"
#include <QtSerialPort/QSerialPortInfo>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        ui->comboBox->addItem(info.portName());
    player = new QMediaPlayer();
    //SlaveThread x;
    //connect(&x, SIGNAL(request(QString)), this, SLOT(showRequest(QString)));
    connect(&thread, SIGNAL(request(QString)), this, SLOT(showRequest(QString)));
    player->setVolume(100);
    //startSlave();
    //connect(&thread, SIGNAL(error(QString)),
   //         this, SLOT(processError(QString)));
    //connect(&thread, SIGNAL(timeout(QString)),
    //        this, SLOT(processTimeout(QString)));
    //x.test();
    //const QString ssss = "ttyACM0";
    //x.startSlave(ssss, 10000, ssss);

    //x.startSlave(ssss, 1000, QSerialPort::Baud9600);
    //void SlaveThread::startSlave(const QString &portName, int waitTimeout, const QString &response)
    //GameWindow w2;
    // cлушать com порт
    flag = false;
    game.setWindowTitle("Угадай мелодию");
    game.show();
    QObject::connect(this, SIGNAL(mysignal(QString)), &game ,SLOT(myslot(QString)));
    QObject::connect(this, SIGNAL(pressitem(int, int)), &game ,SLOT(pressitemslot(int, int)));
    QObject::connect(this, SIGNAL(select(int)), &game ,SLOT(select(int)));
    QObject::connect(this, SIGNAL(supergame(int)), &game ,SLOT(supergame(int)));
    //QObject::connect(super, SIGNAL(timeout()), this ,SLOT(step()));
    //    QString path;
    //QString appdir(a.applicationDirPath());
    QDir x;
    sounds = x.currentPath() + "/mp3/";
    startSlave();
    supergametime = 0;
    timerId = startTimer(1000);
    sflag = false;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete player;
    delete super;
    killTimer(timerId);
}

void MainWindow::pushgamer()
{
    emit(game.addpoint(ui->spinBox->value(), ui->spinBox_2->value()));
    ui->spinBox->setValue(0);
    ui->spinBox_2->setValue(0);
    player->stop();
    player->setMedia(QUrl::fromLocalFile(QDir::toNativeSeparators("C://Projects//melody//mp3//success.mp3")));
    sflag = false;
    player->play();
}

void MainWindow::myslot()
{
//    qDebug() << ((QPushButton*)sender())->text();
//    qDebug() << ((QPushButton*)sender())->whatsThis();
    player->stop();
    QString frombutton;
    frombutton = ((QPushButton*)sender())->whatsThis();
    QStringList LIST;
    LIST=frombutton.split(" ");
    int i = LIST[LIST.count()-2].toInt();
    int j = LIST[LIST.count()-1].toInt();
    j++;
    ui->spinBox_2->setValue(j*10);
    emit(pressitem(i, j));
    frombutton = frombutton.split(".mp3")[0];
    frombutton+=".mp3";
    player->setMedia(QUrl::fromLocalFile(QDir::toNativeSeparators(frombutton)));
    emit(select(0));
    flag = true;
    player->play();
    ((QPushButton*)sender())->setDisabled(true);

    if (ui->spinBoxTime->value() > 0)
    {
        supergametime = ui->spinBoxTime->value();
        sflag = true;
    }
}

void MainWindow::opendir()
{
    if(!(ui->box->isEmpty()))
    {
        QLayoutItem *child;
        while ((child = ui->box->takeAt(0)) != 0)
            delete child->widget();

    }
    QFileDialog dial(this);
    QDir dir,tmpdir;
    QString s = dial.getExistingDirectory(), tmpstr;
    QPushButton* button;
    QLabel *label;
    dir.setPath(s);
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    for(unsigned int i = 0 ; i < dir.count(); i++)
    {
        label = new QLabel(dir[i]);
        ui->box->addWidget(label, i, 0);

        tmpstr = dir[i];
        tmpdir.setPath(s);
        tmpdir.cd(tmpstr);
        tmpdir.setFilter(QDir::Files | QDir::NoDotAndDotDot);

        for(unsigned int j = 0; j < tmpdir.count(); j++)
        {
            button = new QPushButton(tmpdir[j]);
//            qDebug() << tmpdir.absolutePath()+tmpdir[j];
            QObject::connect(button, SIGNAL(clicked()), this,  SLOT(myslot()));
            ui->box->addWidget(button, i, j + 1);
            button->setWhatsThis(tmpdir.absolutePath()+"/"+tmpdir[j]+" "+QString::number(i)+" "+QString::number(j));
            qDebug() << button->whatsThis();
        }
    }
    ui->box->update();
    dir = 0;
    emit(mysignal(s));

}


void MainWindow::showRequest(const QString &s)
{
//    trafficLabel->setText(tr("Traffic, transaction #%1:"
//                             "\n\r-request: %2"
//                             "\n\r-response: %3")
//                          .arg(++transactionCount).arg(s).arg(responseLineEdit->text()));
    //    qDebug() << s.toLatin1().toHex().toInt(0, 10);
    if (flag){
        player->stop();
        flag = false;
        qDebug() << s.toLatin1().toHex().toInt(0, 10);
        emit(ui->spinBox->setValue(s.toLatin1().toHex().toInt(0, 10)));
        emit(select(s.toLatin1().toHex().toInt(0, 10)));
        qDebug() << "OK";
        player->stop();
        //player->setMedia(QUrl::fromLocalFile(QDir::toNativeSeparators("C://Projects//melody//mp3//fanfari.mp3")));
        player->setMedia(QUrl::fromLocalFile(QDir::toNativeSeparators("C://Projects//melody//mp3//push.mp3")));
        player->play();
    }
}


void MainWindow::startSlave()
{
    qDebug() << "ok";
    //runButton->setEnabled(false);
    //statusLabel->setText(tr("Status: Running, connected to port %1.")
    //                     .arg(serialPortComboBox->currentText()));
    thread.startSlave(ui->comboBox->currentText(), 10000);
}


void MainWindow::fanfari()
{
    player->stop();
    player->setMedia(QUrl::fromLocalFile(QDir::toNativeSeparators("C://Projects//melody//mp3//fanfari.mp3")));
    player->play();
}

void MainWindow::endgame()
{
    player->stop();
    player->setMedia(QUrl::fromLocalFile(QDir::toNativeSeparators("C://Projects//melody//mp3//endgame.mp3")));
    player->play();
}


void MainWindow::startSuperGame()
{
    //super->start(1000);
}


void MainWindow::timerEvent(QTimerEvent *event)
{
    if (sflag == true)
    {
        game.supergame(supergametime);
        supergametime--;
        if (supergametime < 0)
        {
            sflag = false;
            player->stop();
        }
    }
    //qDebug() << "step";
}


void MainWindow::fail()
{
    player->stop();
    player->setMedia(QUrl::fromLocalFile(QDir::toNativeSeparators("C://Projects//melody//mp3//fail.mp3")));
    player->play();
    qDebug() << "fail";
}
