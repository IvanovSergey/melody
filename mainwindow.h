#ifndef MAINWINDOW_H
#include "savethread.h"
#include "gamewindow.h"
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDebug>
#include <QMediaPlayer>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public slots:
    void opendir();
    void myslot();
    void showRequest(const QString &s);
    void pushgamer();
    void startSlave();
    void fanfari();
    void endgame();
    void fail();
signals:
    void mysignal(QString);
    void pressitem(int,int);
    void select(int);
    void supergame(int);

public:
    explicit MainWindow(QWidget *parent = 0);
    //void showRequest(const QString &s);
    ~MainWindow();
    //void startSlave();
    SlaveThread thread;
    GameWindow game;
    bool flag;
    QString sounds;
    int supergametime;
    void startSuperGame();
    bool sflag;

private:
    Ui::MainWindow *ui;
    QMediaPlayer* player;
    QTimer *super;
    int timerId;

protected:
    void timerEvent(QTimerEvent *event);
};

#endif // MAINWINDOW_H
